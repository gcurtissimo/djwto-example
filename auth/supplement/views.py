from django.http import JsonResponse


def get_user_token(request):
    response = {"token": None}
    if "jwt_access_token" in request.COOKIES:
        token = request.COOKIES["jwt_access_token"]
        if token:
            response["token"] = token
    return JsonResponse(response)


def get_csrf_token(request):
    response = {"csrf_token": None}
    if "csrftoken" in request.COOKIES:
        token = request.COOKIES["csrftoken"]
        if token:
            response["token"] = token
    return JsonResponse(response)
