from django.urls import path
from .views import get_csrf_token, get_user_token

urlpatterns = [
    path("api/tokens/mine/", get_user_token),
    path("api/csrf/mine/", get_csrf_token),
]
