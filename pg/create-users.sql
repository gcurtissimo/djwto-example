CREATE ROLE "auth" WITH CREATEDB LOGIN PASSWORD 'auth';
CREATE ROLE "data" WITH CREATEDB LOGIN PASSWORD 'data';

CREATE DATABASE "auth" WITH OWNER "auth";
CREATE DATABASE "data" WITH OWNER "data";
