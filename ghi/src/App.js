import { useState, useEffect, useCallback } from 'react';

function Success(props) {
  return (
    <div className="notification is-success">
      {props.children}
    </div>
  );
}

function Warning(props) {
  return (
    <div className="notification is-warning">
      {props.children}
    </div>
  );
}

function Error(props) {
  return (
    <div className="notification is-danger is-light">
      {props.children}
    </div>
  );
}

async function getData(setData, setError, setWarning) {
  let jwt = null;
  const jwtUrl = `${process.env.REACT_APP_AUTH}/api/tokens/mine/`;
  const jwtResponse = await fetch(jwtUrl, {
    credentials: 'include',
  });
  if (jwtResponse.ok) {
    const data = await jwtResponse.json();
    jwt = data.token;
  }
  setData(null);
  setError('');
  const url = `${process.env.REACT_APP_DATA}/api/data/`;
  try {
    const response = await fetch(url, {
      headers: {
        'Authorization': jwt ? `Bearer ${jwt}` : '',
      }
    });
    if (response.ok) {
      const data = await response.json();
      setData(data.values);
      setWarning('');
      setError('');
    } else {
      setError("Log in to get data.");
      setWarning('');
   }
  } catch (e) {
    console.log(e);
    setError("Network error occurred: " + e.message);
  }
}

async function login(setData, setError, setWarning, getData) {
  setError('');
  setWarning('Logging in...');
  const url = `${process.env.REACT_APP_AUTH}/login/`;
  const formData = new FormData();
  formData.append('username', 'example');
  formData.append('password', 'password');
  try {
    const response = await fetch(url, {
      credentials: 'include',
      method: 'post',
      body: formData,
    });
    if (response.ok) {
      const data = await response.json();
      setData(data.values);
      setWarning('');
      setError('');
    } else {
      setWarning('');
      setError("Log in to get data.");
    }
  } catch (e) {
    console.log(e);
    setError("Network error occurred: " + e.message);
  }
  await getData(setData, setError, setWarning);
}

async function logout(setData, setError, setWarning, getData) {
  let csrf = null;
  const csrfUrl = `${process.env.REACT_APP_AUTH}/api/csrf/mine/`;
  const csrfResponse = await fetch(csrfUrl, {
    credentials: 'include',
  });
  if (csrfResponse.ok) {
    const data = await csrfResponse.json();
    csrf = data.token;
  }
  if (!csrf) {
    return setError('You have no CSRF token');
  }
  const url = `${process.env.REACT_APP_AUTH}/api/token/refresh/logout/`;
  try {
    const response = await fetch(url, {
      credentials: 'include',
      method: 'delete',
      headers: {
        'X-CSRFToken': csrf ? csrf : '',
      }
    });
    setData('');
    if (response.ok) {
      setError('');
      setWarning('You logged out, it seems. Try clicking the "Get data" button.');
    } else {
      const data = await response.json();
      setError(data && data.error ? data.error : 'It seems logging out failed, friend.');
      setWarning('');
    }
  } catch (e) {
    console.log(e);
    setError("Network error occurred: " + e.message);
  }
}

function App() {
  const [data, setData] = useState(null);
  const [error, setError] = useState('');
  const [warning, setWarning] = useState('Loading...');

  const getDataCallback = useCallback(() => {
    getData(setData, setError, setWarning);
  }, [setData, setError, setWarning]);

  useEffect(() => {
    getDataCallback();
  }, [getDataCallback])

  return (
    <main className="container content">
      <h1>DJWTO Cookie Example</h1>
      <div className="columns">
        <div className="column is-one-third">
          <p>
            This is a simple site that shows how the
            <code>DELETE</code> verb for the
            <code>/logout/</code> endpoint of the excellent
            DJWTO library fails because the <i>Same Site</i>
            value of the "delete cookie" differs from what
            is configured.
          </p>
          <p>
            Here are instructions to see the bug:
          </p>
          <ol>
            <li>Clear cookies for this domain</li>
            <li>
              Refresh the page and see that you cannot get
              data
            </li>
            <li>
              Click the <b>login</b> button and you can now
              get data
            </li>
            <li>
              Refresh the page and see that you can still
              get data due to these settings:
              <pre><code>{`DJWTO_MODE = "TWO-COOKIES"
DJWTO_SAME_SITE = "None"`}</code></pre>
            </li>
            <li>
              Click the <b>logout</b> button and see that
              you can still get data
            </li>
            <li>
              Refresh the page and see you can still get
              data
            </li>
          </ol>
        </div>
        <div className="column">
          <h2>Accessing Protected Data</h2>
          {data
          ? (<Success>"Successfully loaded data!"</Success>)
          : error
          ? (<Error>{error}</Error>)
          : (<Warning>{warning}</Warning>)}
          <h2>Actions</h2>
          <div className="columns">
            <div className="column is-narrow">
              <button onClick={() => login(setData, setError, setWarning, getData)} className="button is-link">
                Login
              </button>
            </div>
            <div className="column">
              <p>
                Click the login button to log in. The Django service
                that handles authentication is configured with the
                following settings.
              </p>
              <pre><code>{`DJWTO_MODE = "TWO-COOKIES"
DJWTO_SAME_SITE = "None"
DJWTO_ISS_CLAIM = "djwto-example-app"`}</code></pre>.
            </div>
          </div>
          <div className="columns">
            <div className="column is-narrow">
              <button onClick={() => getData(setData, setError, setWarning)} className="button is-info">
                Get data
              </button>
            </div>
            <div className="column">
              <p>
                Click the get data button to try to get data based
                on the cookies that are set for the client. The Django
                service that serves data is configured with the following
                settings.
              </p>
              <pre><code>{`DJWTO_MODE = "JSON"
DJWTO_ISS_CLAIM = "djwto-example-app"`}</code></pre>
              <p>
                The view is protected by <code>jwt_login_required</code>.
              </p>
            </div>
          </div>
          <div className="columns">
            <div className="column is-narrow">
              <button onClick={() => logout(setData, setError, setWarning, getData)} className="button is-danger">
                Logout
              </button>
            </div>
            <div className="column">
              <p>
                Click the logout button to try to logout with the <code>DELETE</code>
                HTTP verb to
                <code>{`${process.env.REACT_APP_AUTH}/api/token/refresh/logout/`}</code>
              </p>
              <p>
                Because the <code>SameSite</code> setting for the cookie is not
                <code>"None"</code>, the cookie is not removed.
              </p>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}

export default App;
