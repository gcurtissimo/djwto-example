# djwto-example

An example for [WillanFuks](https://github.com/WillianFuks),
creator of the excellent
[DJWTO](https://github.com/willianFuks/djwto/) library.

This bug only appears when making cross-origin requests.

## Purpose

As reported in [Delete cookie needs same site
settings](https://github.com/WillianFuks/djwto/issues/8),
when using `DJWTO_SAME_SITE="None"` and cookie-based usage
of DJWTO for a project, the _logout_ functionality does not
work.

This project demonstrates that.

## Steps to reproduce

1. Access the front-end at
   <https://gcurtissimo.gitlab.io/djwto-example>.
1. Note that the API call for data fails
1. Click the "login" button
1. Note that the API call for data succeeds
1. Refresh the page
1. Note that the API call for data succeeds because it is
   using `SAMESITE="None"` and `SECURE=True` for the cookie
   settings
1. Click the logout button (which calls the `DELETE`
   version of the `logout` endpoint)
1. Note that the cookies have not been deleted
   ![screen](./images/cookie-error-message.png)
