from django.http import JsonResponse
import djwto.authentication as auth


@auth.jwt_login_required
def get_data(request):
    data = [
        {"id": 1, "name": "One"},
        {"id": 2, "name": "Two"},
        {"id": 3, "name": "Three"},
        {"id": 4, "name": "Four"},
    ]
    return JsonResponse(
        data={"values": data}
    )
